package main

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
)

// get_token_header: get value from header, remove leading bearer string to ensure we have valid jwt tokens
// we need to do this as oauth2-proxy adds the bearer token to id..
func get_token_from_header_value(v string) string {
	return strings.TrimSpace(strings.Replace(v, "Bearer ", "", 1))
}
// decode_token: decode given token, without verification!!! and any error handling ;-)
func decode_token(t string) jwt.Claims {
	token, _, _ := new(jwt.Parser).ParseUnverified(t, jwt.MapClaims{})
	return token.Claims
}

// marshal_claims: pretty print plz
func marshal_claims(c jwt.Claims) string {
	j, _ := json.MarshalIndent(c, "", "  ")
	return string(j)
}

func hello_world(w http.ResponseWriter, req *http.Request) {

	// get tokens from passed headers
	access_token := get_token_from_header_value(req.Header.Get("X-Auth-Request-Access-Token"))
	id_token := get_token_from_header_value(req.Header.Get("X-Auth-Request-Id-Token"))

	// decode access and id token
	access_claims := decode_token(access_token)
	id_claims := decode_token(id_token)

	// print received info
	fmt.Fprintf(w, "\nHeaders:\n")
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}

	fmt.Fprintf(w, "\n\nAccess Token:\n%s\n", access_token)
	fmt.Fprintf(w, "\n\nID Token:\n%s\n", id_token)

	fmt.Fprintf(w, "\n\nAccess Token Claims:\n%s\n", marshal_claims(access_claims))
	fmt.Fprintf(w, "\n\nID Token Claims:\n%s\n", marshal_claims(id_claims))
}

func main() {
	http.HandleFunc("/", hello_world)
	http.ListenAndServe(":8090", nil)
}

