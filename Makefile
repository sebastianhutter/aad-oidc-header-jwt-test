build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o jwt-test

docker: build
	docker build --pull -t "sebastianhutter/aad-jwt-test-via-oauth2-proxy" .
	docker push sebastianhutter/aad-jwt-test-via-oauth2-proxy