FROM scratch

ADD jwt-test /jwt-test
ENTRYPOINT [ "/jwt-test" ]
